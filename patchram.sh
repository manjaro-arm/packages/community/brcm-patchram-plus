#! /bin/bash

MODEL=$(cat /proc/cpuinfo | grep Model | awk '{print $3,$4,$5,$6,$7,$8}')

if [ "$MODEL" = "Raspberry Pi 3 Model B Rev" ]; then
/usr/bin/brcm_patchram_plus --patchram /usr/lib/firmware/updates/brcm/BCM43430A1.hcd --enable_hci --no2bytes --tosleep=1000000 /dev/ttyAMA0
elif [ "$MODEL" = "Raspberry Pi 400 Rev 1.0 " ] || [ "$MODEL" = "Raspberry Pi 400 Rev 1.1 " ]; then
/usr/bin/brcm_patchram_plus --patchram /usr/lib/firmware/updates/brcm/BCM4345C5.hcd --baudrate 3000000 --enable_hci --use_baudrate_for_download --no2bytes --tosleep=1000000 /dev/ttyAMA0
else
/usr/bin/brcm_patchram_plus --patchram /usr/lib/firmware/updates/brcm/BCM4345C0.hcd --baudrate 3000000 --enable_hci --use_baudrate_for_download --no2bytes --tosleep=1000000 /dev/ttyAMA0
fi
